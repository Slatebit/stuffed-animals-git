package com.slatebit.stuffedanimals.minecraft;

import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.model.ModelWither;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.MathHelper;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class ModelStuffedWither extends ModelWither
{
	private ModelRenderer[] field_82905_a;
	private ModelRenderer[] field_82904_b;

	public ModelStuffedWither()
	{
		this.textureWidth = 64;
		this.textureHeight = 64;
		this.field_82905_a = new ModelRenderer[3];
		this.field_82905_a[0] = new ModelRenderer(this, 0, 16);
		this.field_82905_a[0].addBox(-10.0F, 3.9F, -0.5F, 20, 3, 3);
		this.field_82905_a[1] = new ModelRenderer(this).setTextureSize(this.textureWidth, this.textureHeight);
		this.field_82905_a[1].setRotationPoint(-2.0F, 6.9F, -0.5F);
		this.field_82905_a[1].setTextureOffset(0, 22).addBox(0.0F, 0.0F, 0.0F, 3, 10, 3);
		this.field_82905_a[1].setTextureOffset(24, 22).addBox(-4.0F, 1.5F, 0.5F, 11, 2, 2);
		this.field_82905_a[1].setTextureOffset(24, 22).addBox(-4.0F, 4.0F, 0.5F, 11, 2, 2);
		this.field_82905_a[1].setTextureOffset(24, 22).addBox(-4.0F, 6.5F, 0.5F, 11, 2, 2);
		this.field_82905_a[2] = new ModelRenderer(this, 12, 22);
		this.field_82905_a[2].addBox(0.0F, 0.0F, 0.0F, 3, 6, 3);
		this.field_82904_b = new ModelRenderer[3];
		this.field_82904_b[0] = new ModelRenderer(this, 0, 0);
		this.field_82904_b[0].addBox(-4.0F, -4.0F, -4.0F, 8, 8, 8);
		this.field_82904_b[1] = new ModelRenderer(this, 32, 0);
		this.field_82904_b[1].addBox(-4.0F, -4.0F, -4.0F, 6, 6, 6);
		this.field_82904_b[1].rotationPointX = -8.0F;
		this.field_82904_b[1].rotationPointY = 4.0F;
		this.field_82904_b[2] = new ModelRenderer(this, 32, 0);
		this.field_82904_b[2].addBox(-4.0F, -4.0F, -4.0F, 6, 6, 6);
		this.field_82904_b[2].rotationPointX = 10.0F;
		this.field_82904_b[2].rotationPointY = 4.0F;
	}

	@Override
	public void render(Entity p_78088_1_, float p_78088_2_, float p_78088_3_, float p_78088_4_, float p_78088_5_, float p_78088_6_, float p_78088_7_)
	{
		this.setRotationAngles(p_78088_2_, p_78088_3_, 20, p_78088_5_, p_78088_6_, p_78088_7_, p_78088_1_);
		ModelRenderer[] amodelrenderer = this.field_82904_b;
		int i = amodelrenderer.length;
		int j;
		ModelRenderer modelrenderer;
		for (j = 0; j < i; ++ j)
		{
			modelrenderer = amodelrenderer[j];
			modelrenderer.render(p_78088_7_);
		}
		amodelrenderer = this.field_82905_a;
		i = amodelrenderer.length;
		for (j = 0; j < i; ++ j)
		{
			modelrenderer = amodelrenderer[j];
			modelrenderer.render(p_78088_7_);
		}
	}

	@Override
	public void setRotationAngles(float p_78087_1_, float p_78087_2_, float p_78087_3_, float p_78087_4_, float p_78087_5_, float p_78087_6_, Entity p_78087_7_)
	{
		float f6 = MathHelper.cos(p_78087_3_ * 0.1F);
		this.field_82905_a[2].setRotationPoint(-2.0F, 6.9F + MathHelper.cos(this.field_82905_a[1].rotateAngleX) * 10.0F, -0.5F + MathHelper.sin(this.field_82905_a[1].rotateAngleX) * 10.0F);
		this.field_82905_a[2].rotateAngleX = (0.265F + 0.1F * f6) * (float) Math.PI;
	}

	@Override
	public void setLivingAnimations(EntityLivingBase p_78086_1_, float p_78086_2_, float p_78086_3_, float p_78086_4_)
	{
	}
}