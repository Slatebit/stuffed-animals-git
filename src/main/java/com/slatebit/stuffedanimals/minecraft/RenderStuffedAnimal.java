package com.slatebit.stuffedanimals.minecraft;

import static net.minecraftforge.client.IItemRenderer.ItemRenderType.EQUIPPED;
import static net.minecraftforge.client.IItemRenderer.ItemRendererHelper.BLOCK_3D;
import java.util.UUID;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.model.ModelQuadruped;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.tileentity.TileEntitySkullRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTUtil;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StringUtils;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.MinecraftForgeClient;
import org.lwjgl.opengl.GL11;
import com.mojang.authlib.GameProfile;
import com.slatebit.stuffedanimals.common.EntityStuffedAnimal;
import com.slatebit.stuffedanimals.common.StuffedAnimals;

public class RenderStuffedAnimal extends RenderLiving
{
	private static final ResourceLocation enderDragonEyesTextures = new ResourceLocation("textures/entity/enderdragon/dragon_eyes.png");
	private static final ResourceLocation endermanEyesTexture = new ResourceLocation("textures/entity/enderman/enderman_eyes.png");
	private static final ResourceLocation spiderEyesTexture = new ResourceLocation("textures/entity/spider_eyes.png");

	public RenderStuffedAnimal(ModelBase modelBase, float shadowSize)
	{
		super(modelBase, shadowSize);
		for (int i = 0; i < StuffedAnimals.MODEL_MAP.size() - 2; i ++)
		{
			if (i == 6)
			{
				StuffedAnimals.MODEL_MAP.get(601).isChild = false;
				StuffedAnimals.MODEL_MAP.get(602).isChild = false;
			}
			else if (i == 22)
			{
				StuffedAnimals.MODEL_MAP.get(2201).isChild = false;
				StuffedAnimals.MODEL_MAP.get(2202).isChild = false;
			}
			else
			{
				StuffedAnimals.MODEL_MAP.get(i).isChild = false;
			}
		}
	}

	@Override
	public void doRender(Entity entity, double var2, double var4, double var6, float var8, float var9)
	{
		super.doRender((EntityLiving) entity, var2, var4, var6, var8, var9);
	}

	@Override
	protected float getDeathMaxRotation(EntityLivingBase entity)
	{
		return 0;
	}

	@Override
	protected ResourceLocation getEntityTexture(Entity ent)
	{
		EntityStuffedAnimal stuffedAnimal = (EntityStuffedAnimal) ent;
		int type = stuffedAnimal.type;
		this.scale(type, stuffedAnimal.shouldMirror);
		if (type == 0)
		{
			StuffedAnimals.TEXTURE_MAP.put(0, Minecraft.getMinecraft().thePlayer.getLocationSkin());
		}
		else if (type == 6)
		{
			this.mainModel = StuffedAnimals.MODEL_MAP.get(602);
			return StuffedAnimals.TEXTURE_MAP.get(601);
		}
		else if (type == 11 || type == 12)
		{
			this.mainModel = StuffedAnimals.MODEL_MAP.get(11);
			if (type == 11)
			{
				GL11.glScalef(0.7F, 0.7F, 0.7F);
				GL11.glTranslatef(0F, 0.7F, 0F);
			}
		}
		else if (type == 7 || type == 10)
		{
			this.mainModel = StuffedAnimals.MODEL_MAP.get(type);
			return StuffedAnimals.TEXTURE_MAP.get(stuffedAnimal.texture);
		}
		else if (type == 18)
		{
			GL11.glTranslatef(0F, -1.0F, 0F);
		}
		else if (type == 19 || type == 22)
		{
			GL11.glScalef(2.0F, 2.0F, 2.0F);
			GL11.glTranslatef(0F, -0.75F, 0F);
			if (type == 22)
			{
				this.mainModel = StuffedAnimals.MODEL_MAP.get(2201);
				return StuffedAnimals.TEXTURE_MAP.get(type);
			}
		}
		else if (type == 20)
		{
			GL11.glScalef(1.5F, 1.5F, 1.5F);
			GL11.glTranslatef(0F, -0.5625F, 0F);
		}
		else if (type == 23)
		{
			GL11.glScalef(1.2F, 1.2F, 1.2F);
			GL11.glTranslatef(0F, -0.225F, 0F);
		}
		else if (type == 28)
		{
			GL11.glScalef(0.25F, 0.25F, 0.25F);
			GL11.glTranslatef(0F, 2.5F, 0F);
		}
		this.mainModel = StuffedAnimals.MODEL_MAP.get(type);
		return StuffedAnimals.TEXTURE_MAP.get(type);
	}

	@Override
	protected void renderEquippedItems(EntityLivingBase entityLiving, float par2)
	{
		EntityStuffedAnimal stuffedAnimal = (EntityStuffedAnimal) entityLiving;
		if (stuffedAnimal.type == 3)
		{
			this.bindTexture(TextureMap.locationBlocksTexture);
			GL11.glEnable(GL11.GL_CULL_FACE);
			GL11.glPushMatrix();
			GL11.glScalef(1.0F, -1.0F, 1.0F);
			GL11.glTranslatef(0.2F, 0.4F, 0.5F);
			GL11.glRotatef(42.0F, 0.0F, 1.0F, 0.0F);
			this.field_147909_c.renderBlockAsItem(Blocks.red_mushroom, 0, 1.0F);
			GL11.glTranslatef(0.1F, 0.0F, -0.6F);
			GL11.glRotatef(42.0F, 0.0F, 1.0F, 0.0F);
			this.field_147909_c.renderBlockAsItem(Blocks.red_mushroom, 0, 1.0F);
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			((ModelQuadruped) this.mainModel).head.postRender(0.0625F);
			GL11.glScalef(1.0F, -1.0F, 1.0F);
			GL11.glTranslatef(0.0F, 0.75F, -0.2F);
			GL11.glRotatef(12.0F, 0.0F, 1.0F, 0.0F);
			this.field_147909_c.renderBlockAsItem(Blocks.red_mushroom, 0, 1.0F);
			GL11.glPopMatrix();
			GL11.glDisable(GL11.GL_CULL_FACE);
		}
		else if (stuffedAnimal.type == 15 || stuffedAnimal.type == 21 || stuffedAnimal.type == 23)
		{
			GL11.glColor3f(1.0F, 1.0F, 1.0F);
			ItemStack itemstack = stuffedAnimal.getHeldItem();
			ItemStack itemstack1 = stuffedAnimal.func_130225_q(3);
			Item item;
			float f1;
			if (itemstack1 != null)
			{
				GL11.glPushMatrix();
				((ModelBiped) this.mainModel).bipedHead.postRender(0.0625F);
				item = itemstack1.getItem();
				net.minecraftforge.client.IItemRenderer customRenderer = net.minecraftforge.client.MinecraftForgeClient.getItemRenderer(itemstack1, net.minecraftforge.client.IItemRenderer.ItemRenderType.EQUIPPED);
				boolean is3D = customRenderer != null && customRenderer.shouldUseRenderHelper(net.minecraftforge.client.IItemRenderer.ItemRenderType.EQUIPPED, itemstack1, net.minecraftforge.client.IItemRenderer.ItemRendererHelper.BLOCK_3D);
				if (item instanceof ItemBlock)
				{
					if (is3D || RenderBlocks.renderItemIn3d(Block.getBlockFromItem(item).getRenderType()))
					{
						f1 = 0.625F;
						GL11.glTranslatef(0.0F, -0.25F, 0.0F);
						GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
						GL11.glScalef(f1, -f1, -f1);
					}
					this.renderManager.itemRenderer.renderItem(stuffedAnimal, itemstack1, 0);
				}
				else if (item == Items.skull)
				{
					f1 = 1.0625F;
					GL11.glScalef(f1, -f1, -f1);
					GameProfile gameprofile = null;
					if (itemstack1.hasTagCompound())
					{
						NBTTagCompound nbttagcompound = itemstack1.getTagCompound();
						if (nbttagcompound.hasKey("SkullOwner", 10))
						{
							gameprofile = NBTUtil.func_152459_a(nbttagcompound.getCompoundTag("SkullOwner"));
						}
						else if (nbttagcompound.hasKey("SkullOwner", 8) && !StringUtils.isNullOrEmpty(nbttagcompound.getString("SkullOwner")))
						{
							gameprofile = new GameProfile((UUID) null, nbttagcompound.getString("SkullOwner"));
						}
					}
					TileEntitySkullRenderer.field_147536_b.func_152674_a(-0.5F, 0.0F, -0.5F, 1, 180.0F, itemstack1.getItemDamage(), gameprofile);
				}
				GL11.glPopMatrix();
			}
			if (itemstack != null && itemstack.getItem() != null)
			{
				item = itemstack.getItem();
				GL11.glPushMatrix();
				if (this.mainModel.isChild)
				{
					f1 = 0.5F;
					GL11.glTranslatef(0.0F, 0.625F, 0.0F);
					GL11.glRotatef(-20.0F, -1.0F, 0.0F, 0.0F);
					GL11.glScalef(f1, f1, f1);
				}
				((ModelBiped) this.mainModel).bipedRightArm.postRender(0.0625F);
				GL11.glTranslatef(-0.0625F, 0.4375F, 0.0625F);
				net.minecraftforge.client.IItemRenderer customRenderer = net.minecraftforge.client.MinecraftForgeClient.getItemRenderer(itemstack, net.minecraftforge.client.IItemRenderer.ItemRenderType.EQUIPPED);
				boolean is3D = customRenderer != null && customRenderer.shouldUseRenderHelper(net.minecraftforge.client.IItemRenderer.ItemRenderType.EQUIPPED, itemstack, net.minecraftforge.client.IItemRenderer.ItemRendererHelper.BLOCK_3D);
				if (item instanceof ItemBlock && (is3D || RenderBlocks.renderItemIn3d(Block.getBlockFromItem(item).getRenderType())))
				{
					f1 = 0.5F;
					GL11.glTranslatef(0.0F, 0.1875F, -0.3125F);
					f1 *= 0.75F;
					GL11.glRotatef(20.0F, 1.0F, 0.0F, 0.0F);
					GL11.glRotatef(45.0F, 0.0F, 1.0F, 0.0F);
					GL11.glScalef(-f1, -f1, f1);
				}
				else if (item == Items.bow)
				{
					f1 = 0.625F;
					GL11.glTranslatef(0.0F, 0.125F, 0.3125F);
					GL11.glRotatef(-20.0F, 0.0F, 1.0F, 0.0F);
					GL11.glScalef(f1, -f1, f1);
					GL11.glRotatef(-100.0F, 1.0F, 0.0F, 0.0F);
					GL11.glRotatef(45.0F, 0.0F, 1.0F, 0.0F);
				}
				else if (item.isFull3D())
				{
					f1 = 0.625F;
					if (item.shouldRotateAroundWhenRendering())
					{
						GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
						GL11.glTranslatef(0.0F, -0.125F, 0.0F);
					}
					GL11.glTranslatef(0.0F, 0.1875F, 0.0F);
					GL11.glScalef(f1, -f1, f1);
					GL11.glRotatef(-100.0F, 1.0F, 0.0F, 0.0F);
					GL11.glRotatef(45.0F, 0.0F, 1.0F, 0.0F);
				}
				else
				{
					f1 = 0.375F;
					GL11.glTranslatef(0.25F, 0.1875F, -0.1875F);
					GL11.glScalef(f1, f1, f1);
					GL11.glRotatef(60.0F, 0.0F, 0.0F, 1.0F);
					GL11.glRotatef(-90.0F, 1.0F, 0.0F, 0.0F);
					GL11.glRotatef(20.0F, 0.0F, 0.0F, 1.0F);
				}
				float f2;
				int i;
				float f5;
				if (itemstack.getItem().requiresMultipleRenderPasses())
				{
					for (i = 0; i < itemstack.getItem().getRenderPasses(itemstack.getItemDamage()); ++ i)
					{
						int j = itemstack.getItem().getColorFromItemStack(itemstack, i);
						f5 = (j >> 16 & 255) / 255.0F;
						f2 = (j >> 8 & 255) / 255.0F;
						float f3 = (j & 255) / 255.0F;
						GL11.glColor4f(f5, f2, f3, 1.0F);
						this.renderManager.itemRenderer.renderItem(stuffedAnimal, itemstack, i);
					}
				}
				else
				{
					i = itemstack.getItem().getColorFromItemStack(itemstack, 0);
					float f4 = (i >> 16 & 255) / 255.0F;
					f5 = (i >> 8 & 255) / 255.0F;
					f2 = (i & 255) / 255.0F;
					GL11.glColor4f(f4, f5, f2, 1.0F);
					this.renderManager.itemRenderer.renderItem(stuffedAnimal, itemstack, 0);
				}
				GL11.glPopMatrix();
			}
		}
		else if (stuffedAnimal.type == 26)
		{
			ItemStack itemstack = new ItemStack(Blocks.pumpkin, 1);
			if (itemstack.getItem() instanceof ItemBlock)
			{
				GL11.glPushMatrix();
				((ModelStuffedSnowGolem) this.mainModel).head.postRender(0.0625F);
				IItemRenderer customRenderer = MinecraftForgeClient.getItemRenderer(itemstack, EQUIPPED);
				boolean is3D = customRenderer != null && customRenderer.shouldUseRenderHelper(EQUIPPED, itemstack, BLOCK_3D);
				if (is3D || RenderBlocks.renderItemIn3d(Block.getBlockFromItem(itemstack.getItem()).getRenderType()))
				{
					float f1 = 0.625F;
					GL11.glTranslatef(0.0F, -0.34375F, 0.0F);
					GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
					GL11.glScalef(f1, -f1, f1);
				}
				this.renderManager.itemRenderer.renderItem(stuffedAnimal, itemstack, 0);
				GL11.glPopMatrix();
			}
		}
	}

	@Override
	protected int shouldRenderPass(EntityLivingBase entity, int par2, float par3)
	{
		EntityStuffedAnimal stuffedAnimal = (EntityStuffedAnimal) entity;
		if (stuffedAnimal.type == 6)
		{
			this.setRenderPassModel(StuffedAnimals.MODEL_MAP.get(601));
			this.bindTexture(StuffedAnimals.TEXTURE_MAP.get(602));
			return 1;
		}
		else if (stuffedAnimal.type == 11 || stuffedAnimal.type == 12)
		{
			this.setRenderPassModel(StuffedAnimals.MODEL_MAP.get(11));
			this.bindTexture(RenderStuffedAnimal.spiderEyesTexture);
			GL11.glEnable(GL11.GL_BLEND);
			GL11.glDisable(GL11.GL_ALPHA_TEST);
			GL11.glBlendFunc(GL11.GL_ONE, GL11.GL_ONE);
			if (entity.isInvisible())
			{
				GL11.glDepthMask(false);
			}
			else
			{
				GL11.glDepthMask(true);
			}
			char c0 = 61680;
			int j = c0 % 65536;
			int k = c0 / 65536;
			OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, j / 1.0F, k / 1.0F);
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			return 1;
		}
		else if (stuffedAnimal.type == 13)
		{
			this.setRenderPassModel(this.mainModel);
			this.bindTexture(RenderStuffedAnimal.endermanEyesTexture);
			float f1 = 1.0F;
			GL11.glEnable(GL11.GL_BLEND);
			GL11.glDisable(GL11.GL_ALPHA_TEST);
			GL11.glBlendFunc(GL11.GL_ONE, GL11.GL_ONE);
			GL11.glDisable(GL11.GL_LIGHTING);
			if (entity.isInvisible())
			{
				GL11.glDepthMask(false);
			}
			else
			{
				GL11.glDepthMask(true);
			}
			char c0 = 61680;
			int j = c0 % 65536;
			int k = c0 / 65536;
			OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, j / 1.0F, k / 1.0F);
			GL11.glEnable(GL11.GL_LIGHTING);
			GL11.glColor4f(1.0F, 1.0F, 1.0F, f1);
			return 1;
		}
		else if (stuffedAnimal.type == 22)
		{
			this.setRenderPassModel(StuffedAnimals.MODEL_MAP.get(2202));
			if (par2 == 0)
			{
				GL11.glEnable(GL11.GL_NORMALIZE);
				GL11.glEnable(GL11.GL_BLEND);
				GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
				return 1;
			}
			else
			{
				if (par2 == 1)
				{
					GL11.glDisable(GL11.GL_BLEND);
					GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
				}
				return -1;
			}
		}
		else if (stuffedAnimal.type == 28)
		{
			this.setRenderPassModel(this.mainModel);
			if (par2 == 1)
			{
				GL11.glDepthFunc(GL11.GL_LEQUAL);
			}
			if (par2 != 0)
			{
				return -1;
			}
			else
			{
				this.bindTexture(RenderStuffedAnimal.enderDragonEyesTextures);
				GL11.glEnable(GL11.GL_BLEND);
				GL11.glDisable(GL11.GL_ALPHA_TEST);
				GL11.glBlendFunc(GL11.GL_ONE, GL11.GL_ONE);
				GL11.glDisable(GL11.GL_LIGHTING);
				GL11.glDepthFunc(GL11.GL_EQUAL);
				char c0 = 61680;
				int j = c0 % 65536;
				int k = c0 / 65536;
				OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, j / 1.0F, k / 1.0F);
				GL11.glEnable(GL11.GL_LIGHTING);
				GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
				return 1;
			}
		}
		return -1;
	}

	private void scale(int type, boolean shouldMirror)
	{
		if (shouldMirror)
		{
			GL11.glScalef(-0.5F, 0.5F, -0.5F);
		}
		else
		{
			GL11.glScalef(0.5F, 0.5F, 0.5F);
		}
		GL11.glTranslatef(0F, 1.5F, 0F);
		this.shadowSize = StuffedAnimals.SHADOW_MAP.get(type);
	}
}
