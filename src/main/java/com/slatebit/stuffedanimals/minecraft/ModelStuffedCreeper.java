package com.slatebit.stuffedanimals.minecraft;

import net.minecraft.client.model.ModelCreeper;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class ModelStuffedCreeper extends ModelCreeper
{
	public ModelRenderer head;
	public ModelRenderer field_78133_b;
	public ModelRenderer body;
	public ModelRenderer leg1;
	public ModelRenderer leg2;
	public ModelRenderer leg3;
	public ModelRenderer leg4;

	public ModelStuffedCreeper()
	{
		this(0.0F);
	}

	public ModelStuffedCreeper(float p_i1147_1_)
	{
		byte b0 = 4;
		this.head = new ModelRenderer(this, 0, 0);
		this.head.addBox(-4.0F, -8.0F, -4.0F, 8, 8, 8, p_i1147_1_);
		this.head.setRotationPoint(0.0F, b0, 0.0F);
		this.field_78133_b = new ModelRenderer(this, 32, 0);
		this.field_78133_b.addBox(-4.0F, -8.0F, -4.0F, 8, 8, 8, p_i1147_1_ + 0.5F);
		this.field_78133_b.setRotationPoint(0.0F, b0, 0.0F);
		this.body = new ModelRenderer(this, 16, 16);
		this.body.addBox(-4.0F, 0.0F, -2.0F, 8, 12, 4, p_i1147_1_);
		this.body.setRotationPoint(0.0F, b0, 0.0F);
		this.leg1 = new ModelRenderer(this, 0, 16);
		this.leg1.addBox(-2.0F, 0.0F, -2.0F, 4, 6, 4, p_i1147_1_);
		this.leg1.setRotationPoint(-2.0F, 12 + b0, 4.0F);
		this.leg2 = new ModelRenderer(this, 0, 16);
		this.leg2.addBox(-2.0F, 0.0F, -2.0F, 4, 6, 4, p_i1147_1_);
		this.leg2.setRotationPoint(2.0F, 12 + b0, 4.0F);
		this.leg3 = new ModelRenderer(this, 0, 16);
		this.leg3.addBox(-2.0F, 0.0F, -2.0F, 4, 6, 4, p_i1147_1_);
		this.leg3.setRotationPoint(-2.0F, 12 + b0, -4.0F);
		this.leg4 = new ModelRenderer(this, 0, 16);
		this.leg4.addBox(-2.0F, 0.0F, -2.0F, 4, 6, 4, p_i1147_1_);
		this.leg4.setRotationPoint(2.0F, 12 + b0, -4.0F);
	}

	@Override
	public void setRotationAngles(float p_78087_1_, float p_78087_2_, float p_78087_3_, float p_78087_4_, float p_78087_5_, float p_78087_6_, Entity p_78087_7_)
	{
	}
}