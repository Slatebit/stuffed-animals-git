package com.slatebit.stuffedanimals.minecraft;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;

public class ModelStuffedOcelot extends ModelBase
{
	ModelRenderer ocelotBackLeftLeg;
	ModelRenderer ocelotBackRightLeg;
	ModelRenderer ocelotFrontLeftLeg;
	ModelRenderer ocelotFrontRightLeg;
	ModelRenderer ocelotTail;
	ModelRenderer ocelotTail2;
	ModelRenderer ocelotHead;
	ModelRenderer ocelotBody;

	public ModelStuffedOcelot()
	{
		this.setTextureOffset("head.main", 0, 0);
		this.setTextureOffset("head.nose", 0, 24);
		this.setTextureOffset("head.ear1", 0, 10);
		this.setTextureOffset("head.ear2", 6, 10);
		this.ocelotHead = new ModelRenderer(this, "head");
		this.ocelotHead.addBox("main", -2.5F, -2.0F, -3.0F, 5, 4, 5);
		this.ocelotHead.addBox("nose", -1.5F, 0.0F, -4.0F, 3, 2, 2);
		this.ocelotHead.addBox("ear1", -2.0F, -3.0F, 0.0F, 1, 1, 2);
		this.ocelotHead.addBox("ear2", 1.0F, -3.0F, 0.0F, 1, 1, 2);
		this.ocelotHead.setRotationPoint(0.0F, 15.0F, -9.0F);
		this.ocelotBody = new ModelRenderer(this, 20, 0);
		this.ocelotBody.addBox(-2.0F, 3.0F, -8.0F, 4, 16, 6, 0.0F);
		this.ocelotBody.setRotationPoint(0.0F, 12.0F, -10.0F);
		this.ocelotTail = new ModelRenderer(this, 0, 15);
		this.ocelotTail.addBox(-0.5F, 0.0F, 0.0F, 1, 8, 1);
		this.ocelotTail.rotateAngleX = 0.9F;
		this.ocelotTail.setRotationPoint(0.0F, 15.0F, 8.0F);
		this.ocelotTail2 = new ModelRenderer(this, 4, 15);
		this.ocelotTail2.addBox(-0.5F, 0.0F, 0.0F, 1, 4, 1);
		this.ocelotTail2.setRotationPoint(0.0F, 20.0F, 14.0F);
		this.ocelotBackLeftLeg = new ModelRenderer(this, 8, 13);
		this.ocelotBackLeftLeg.addBox(-1.0F, 0.0F, 1.0F, 2, 6, 2);
		this.ocelotBackLeftLeg.setRotationPoint(1.1F, 18.0F, 5.0F);
		this.ocelotBackRightLeg = new ModelRenderer(this, 8, 13);
		this.ocelotBackRightLeg.addBox(-1.0F, 0.0F, 1.0F, 2, 6, 2);
		this.ocelotBackRightLeg.setRotationPoint(-1.1F, 18.0F, 5.0F);
		this.ocelotFrontLeftLeg = new ModelRenderer(this, 40, 0);
		this.ocelotFrontLeftLeg.addBox(-1.0F, 0.0F, 0.0F, 2, 10, 2);
		this.ocelotFrontLeftLeg.setRotationPoint(1.2F, 13.8F, -5.0F);
		this.ocelotFrontRightLeg = new ModelRenderer(this, 40, 0);
		this.ocelotFrontRightLeg.addBox(-1.0F, 0.0F, 0.0F, 2, 10, 2);
		this.ocelotFrontRightLeg.setRotationPoint(-1.2F, 13.8F, -5.0F);
	}

	@Override
	public void render(Entity par1Entity, float par2, float par3, float par4, float par5, float par6, float par7)
	{
		this.setRotationAngles(par2, par3, par4, par5, par6, par7, par1Entity);
		this.ocelotHead.render(par7);
		this.ocelotBody.render(par7);
		this.ocelotTail.render(par7);
		this.ocelotTail2.render(par7);
		this.ocelotBackLeftLeg.render(par7);
		this.ocelotBackRightLeg.render(par7);
		this.ocelotFrontLeftLeg.render(par7);
		this.ocelotFrontRightLeg.render(par7);
	}

	@Override
	public void setRotationAngles(float par1, float par2, float par3, float par4, float par5, float par6, Entity par7Entity)
	{
		this.ocelotBody.rotateAngleX = (float) Math.PI / 2F;
	}

	@Override
	public void setLivingAnimations(EntityLivingBase par1EntityLivingBase, float par2, float par3, float par4)
	{
	}
}
