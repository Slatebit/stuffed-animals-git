package com.slatebit.stuffedanimals.minecraft;

import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class ModelStuffedZombieVillager extends ModelStuffedBiped
{
	public ModelStuffedZombieVillager()
	{
		this(0.0F, 0.0F, false);
	}

	public ModelStuffedZombieVillager(float p_i1165_1_, float p_i1165_2_, boolean p_i1165_3_)
	{
		super(p_i1165_1_, 0.0F, 64, p_i1165_3_ ? 32 : 64);
		if (p_i1165_3_)
		{
			this.bipedHead = new ModelRenderer(this, 0, 0);
			this.bipedHead.addBox(-4.0F, -10.0F, -4.0F, 8, 6, 8, p_i1165_1_);
			this.bipedHead.setRotationPoint(0.0F, 0.0F + p_i1165_2_, 0.0F);
		}
		else
		{
			this.bipedHead = new ModelRenderer(this);
			this.bipedHead.setRotationPoint(0.0F, 0.0F + p_i1165_2_, 0.0F);
			this.bipedHead.setTextureOffset(0, 32).addBox(-4.0F, -10.0F, -4.0F, 8, 10, 8, p_i1165_1_);
			this.bipedHead.setTextureOffset(24, 32).addBox(-1.0F, -3.0F, -6.0F, 2, 4, 2, p_i1165_1_);
		}
	}

	@Override
	public void setRotationAngles(float p_78087_1_, float p_78087_2_, float p_78087_3_, float p_78087_4_, float p_78087_5_, float p_78087_6_, Entity p_78087_7_)
	{
		this.bipedRightArm.rotateAngleX = -((float) Math.PI / 2F);
		this.bipedLeftArm.rotateAngleX = -((float) Math.PI / 2F);
	}
}