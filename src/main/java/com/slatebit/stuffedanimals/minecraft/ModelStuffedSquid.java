package com.slatebit.stuffedanimals.minecraft;

import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.model.ModelSquid;
import net.minecraft.entity.Entity;

public class ModelStuffedSquid extends ModelSquid
{
	ModelRenderer squidBody;
	ModelRenderer[] squidTentacles = new ModelRenderer[8];

	public ModelStuffedSquid()
	{
		byte b0 = -16;
		this.squidBody = new ModelRenderer(this, 0, 0);
		this.squidBody.addBox(-6.0F, -8.0F, -6.0F, 12, 16, 12);
		this.squidBody.rotationPointY += 24 + b0;
		for (int i = 0; i < this.squidTentacles.length; ++ i)
		{
			this.squidTentacles[i] = new ModelRenderer(this, 48, 0);
			double d0 = i * Math.PI * 2.0D / this.squidTentacles.length;
			float f = (float) Math.cos(d0) * 5.0F;
			float f1 = (float) Math.sin(d0) * 5.0F;
			this.squidTentacles[i].addBox(-1.01F, 0.0F, -1.01F, 2, 8, 2);
			this.squidTentacles[i].rotationPointX = f;
			this.squidTentacles[i].rotationPointZ = f1;
			this.squidTentacles[i].rotationPointY = 31 + b0;
			d0 = i * Math.PI * -2.0D / this.squidTentacles.length + Math.PI / 2D;
			this.squidTentacles[i].rotateAngleY = (float) d0;
		}
	}

	@Override
	public void render(Entity par1Entity, float par2, float par3, float par4, float par5, float par6, float par7)
	{
		this.squidBody.render(par7);
		for (int i = 0; i < this.squidTentacles.length; ++ i)
		{
			this.squidTentacles[i].render(par7);
		}
	}
}
