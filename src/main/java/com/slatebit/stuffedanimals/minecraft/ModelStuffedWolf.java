package com.slatebit.stuffedanimals.minecraft;

import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.model.ModelWolf;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class ModelStuffedWolf extends ModelWolf
{
	public ModelRenderer wolfHeadMain;
	public ModelRenderer wolfBody;
	public ModelRenderer wolfLeg1;
	public ModelRenderer wolfLeg2;
	public ModelRenderer wolfLeg3;
	public ModelRenderer wolfLeg4;
	ModelRenderer wolfTail;
	ModelRenderer wolfMane;

	public ModelStuffedWolf()
	{
		float f = 0.0F;
		float f1 = 13.5F;
		this.wolfHeadMain = new ModelRenderer(this, 0, 0);
		this.wolfHeadMain.addBox(-3.0F, -3.0F, -2.0F, 6, 6, 4, f);
		this.wolfHeadMain.setRotationPoint(-1.0F, f1, -7.0F);
		this.wolfBody = new ModelRenderer(this, 18, 14);
		this.wolfBody.addBox(-4.0F, -2.0F, -3.0F, 6, 9, 6, f);
		this.wolfBody.setRotationPoint(0.0F, 14.0F, 2.0F);
		this.wolfMane = new ModelRenderer(this, 21, 0);
		this.wolfMane.addBox(-4.0F, -3.0F, -3.0F, 8, 6, 7, f);
		this.wolfMane.setRotationPoint(-1.0F, 14.0F, 2.0F);
		this.wolfLeg1 = new ModelRenderer(this, 0, 18);
		this.wolfLeg1.addBox(-1.0F, 0.0F, -1.0F, 2, 8, 2, f);
		this.wolfLeg1.setRotationPoint(-2.5F, 16.0F, 7.0F);
		this.wolfLeg2 = new ModelRenderer(this, 0, 18);
		this.wolfLeg2.addBox(-1.0F, 0.0F, -1.0F, 2, 8, 2, f);
		this.wolfLeg2.setRotationPoint(0.5F, 16.0F, 7.0F);
		this.wolfLeg3 = new ModelRenderer(this, 0, 18);
		this.wolfLeg3.addBox(-1.0F, 0.0F, -1.0F, 2, 8, 2, f);
		this.wolfLeg3.setRotationPoint(-2.5F, 16.0F, -4.0F);
		this.wolfLeg4 = new ModelRenderer(this, 0, 18);
		this.wolfLeg4.addBox(-1.0F, 0.0F, -1.0F, 2, 8, 2, f);
		this.wolfLeg4.setRotationPoint(0.5F, 16.0F, -4.0F);
		this.wolfTail = new ModelRenderer(this, 9, 18);
		this.wolfTail.addBox(-1.0F, 0.0F, -1.0F, 2, 8, 2, f);
		this.wolfTail.setRotationPoint(-1.0F, 12.0F, 8.0F);
		this.wolfHeadMain.setTextureOffset(16, 14).addBox(-3.0F, -5.0F, 0.0F, 2, 2, 1, f);
		this.wolfHeadMain.setTextureOffset(16, 14).addBox(1.0F, -5.0F, 0.0F, 2, 2, 1, f);
		this.wolfHeadMain.setTextureOffset(0, 10).addBox(-1.5F, 0.0F, -5.0F, 3, 3, 4, f);
	}

	@Override
	public void render(Entity p_78088_1_, float p_78088_2_, float p_78088_3_, float p_78088_4_, float p_78088_5_, float p_78088_6_, float p_78088_7_)
	{
		this.setRotationAngles(p_78088_2_, p_78088_3_, p_78088_4_, p_78088_5_, p_78088_6_, p_78088_7_, p_78088_1_);
		this.wolfHeadMain.render(p_78088_7_);
		this.wolfBody.render(p_78088_7_);
		this.wolfLeg1.render(p_78088_7_);
		this.wolfLeg2.render(p_78088_7_);
		this.wolfLeg3.render(p_78088_7_);
		this.wolfLeg4.render(p_78088_7_);
		this.wolfTail.render(p_78088_7_);
		this.wolfMane.render(p_78088_7_);
	}

	@Override
	public void setLivingAnimations(EntityLivingBase p_78086_1_, float p_78086_2_, float p_78086_3_, float p_78086_4_)
	{
		this.wolfTail.rotateAngleX = (float) Math.PI / 2.5F;
		this.wolfMane.setRotationPoint(-1.0F, 16.0F, -3.0F);
		this.wolfMane.rotateAngleX = (float) Math.PI * 2F / 5F;
		this.wolfMane.rotateAngleY = 0.0F;
		this.wolfBody.setRotationPoint(0.0F, 18.0F, 0.0F);
		this.wolfBody.rotateAngleX = (float) Math.PI / 4F;
		this.wolfTail.setRotationPoint(-1.0F, 21.0F, 6.0F);
		this.wolfLeg1.setRotationPoint(-2.5F, 22.0F, 2.0F);
		this.wolfLeg1.rotateAngleX = (float) Math.PI * 3F / 2F;
		this.wolfLeg2.setRotationPoint(0.5F, 22.0F, 2.0F);
		this.wolfLeg2.rotateAngleX = (float) Math.PI * 3F / 2F;
		this.wolfLeg3.rotateAngleX = 5.811947F;
		this.wolfLeg3.setRotationPoint(-2.49F, 17.0F, -4.0F);
		this.wolfLeg4.rotateAngleX = 5.811947F;
		this.wolfLeg4.setRotationPoint(0.51F, 17.0F, -4.0F);
	}
}