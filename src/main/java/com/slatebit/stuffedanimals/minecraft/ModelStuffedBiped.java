package com.slatebit.stuffedanimals.minecraft;

import net.minecraft.client.model.ModelBiped;
import net.minecraft.entity.Entity;

public class ModelStuffedBiped extends ModelBiped
{
	public ModelStuffedBiped()
	{
		super();
	}

	public ModelStuffedBiped(float p_i1167_1_, float p_i1167_2_, int p_i1167_3_, int p_i1167_4_)
	{
		super(p_i1167_1_, p_i1167_2_, p_i1167_3_, p_i1167_3_);
	}

	@Override
	public void setRotationAngles(float par1, float par2, float par3, float par4, float par5, float par6, Entity par7Entity)
	{
	}
}
