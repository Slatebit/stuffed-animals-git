package com.slatebit.stuffedanimals.minecraft;

import net.minecraft.client.model.ModelMagmaCube;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class ModelStuffedMagmaCube extends ModelMagmaCube
{
	ModelRenderer[] field_78109_a = new ModelRenderer[8];
	ModelRenderer field_78108_b;

	public ModelStuffedMagmaCube()
	{
		for (int i = 0; i < this.field_78109_a.length; ++ i)
		{
			byte b0 = 0;
			int j = i;
			if (i == 2)
			{
				b0 = 24;
				j = 10;
			}
			else if (i == 3)
			{
				b0 = 24;
				j = 19;
			}
			this.field_78109_a[i] = new ModelRenderer(this, b0, j);
			this.field_78109_a[i].addBox(-4.0F, 16 + i, -4.0F, 8, 1, 8);
		}
		this.field_78108_b = new ModelRenderer(this, 0, 16);
		this.field_78108_b.addBox(-2.0F, 18.0F, -2.0F, 4, 4, 4);
	}

	@Override
	public void setLivingAnimations(EntityLivingBase p_78086_1_, float p_78086_2_, float p_78086_3_, float p_78086_4_)
	{
	}

	@Override
	public void render(Entity p_78088_1_, float p_78088_2_, float p_78088_3_, float p_78088_4_, float p_78088_5_, float p_78088_6_, float p_78088_7_)
	{
		this.setRotationAngles(p_78088_2_, p_78088_3_, p_78088_4_, p_78088_5_, p_78088_6_, p_78088_7_, p_78088_1_);
		this.field_78108_b.render(p_78088_7_);
		for (int i = 0; i < this.field_78109_a.length; ++ i)
		{
			this.field_78109_a[i].render(p_78088_7_);
		}
	}
}