package com.slatebit.stuffedanimals.minecraft;

import net.minecraft.entity.Entity;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class ModelStuffedZombie extends ModelStuffedBiped
{
	public ModelStuffedZombie()
	{
		this(0.0F, false);
	}

	protected ModelStuffedZombie(float p_i1167_1_, float p_i1167_2_, int p_i1167_3_, int p_i1167_4_)
	{
		super(p_i1167_1_, p_i1167_2_, p_i1167_3_, p_i1167_4_);
	}

	public ModelStuffedZombie(float p_i1168_1_, boolean p_i1168_2_)
	{
		super(p_i1168_1_, 0.0F, 64, p_i1168_2_ ? 32 : 64);
	}

	@Override
	public void setRotationAngles(float p_78087_1_, float p_78087_2_, float p_78087_3_, float p_78087_4_, float p_78087_5_, float p_78087_6_, Entity p_78087_7_)
	{
		this.bipedRightArm.rotateAngleX = -((float) Math.PI / 2F);
		this.bipedLeftArm.rotateAngleX = -((float) Math.PI / 2F);
	}
}