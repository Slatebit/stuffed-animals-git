package com.slatebit.stuffedanimals.minecraft;

import net.minecraft.client.model.ModelBiped;
import com.slatebit.stuffedanimals.common.CommonProxy;
import com.slatebit.stuffedanimals.common.EntityStuffedAnimal;
import cpw.mods.fml.client.registry.RenderingRegistry;

public class ClientProxy extends CommonProxy
{
	@Override
	public void addRenders()
	{
		RenderingRegistry.registerEntityRenderingHandler(EntityStuffedAnimal.class, new RenderStuffedAnimal(new ModelBiped(), 1));
	}
}
