package com.slatebit.stuffedanimals.minecraft;

import net.minecraft.client.model.ModelQuadruped;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelStuffedQuadruped extends ModelQuadruped
{
	public ModelStuffedQuadruped(int par1, float par2)
	{
		super(par1, par2);
		if (par1 == 6)
		{
			this.head.setTextureOffset(16, 16).addBox(-2.0F, 0.0F, -9.0F, 4, 3, 1, 0);
		}
		else if (par1 == 12)
		{
			this.head = new ModelRenderer(this, 0, 0);
			this.head.addBox(-4.0F, -4.0F, -6.0F, 8, 8, 6, 0.0F);
			this.head.setRotationPoint(0.0F, 4.0F, -8.0F);
			this.head.setTextureOffset(22, 0).addBox(-5.0F, -5.0F, -4.0F, 1, 3, 1, 0.0F);
			this.head.setTextureOffset(22, 0).addBox(4.0F, -5.0F, -4.0F, 1, 3, 1, 0.0F);
			this.body = new ModelRenderer(this, 18, 4);
			this.body.addBox(-6.0F, -10.0F, -7.0F, 12, 18, 10, 0.0F);
			this.body.setRotationPoint(0.0F, 5.0F, 2.0F);
			this.body.setTextureOffset(52, 0).addBox(-2.0F, 2.0F, -8.0F, 4, 6, 1);
		}
	}

	public ModelStuffedQuadruped(int par1, float par2, int isSheep)
	{
		super(par1, par2);
		if (isSheep == 1)
		{
			this.head = new ModelRenderer(this, 0, 0);
			this.head.addBox(-3.0F, -4.0F, -4.0F, 6, 6, 6, 0.6F);
			this.head.setRotationPoint(0.0F, 6.0F, -8.0F);
			this.body = new ModelRenderer(this, 28, 8);
			this.body.addBox(-4.0F, -10.0F, -7.0F, 8, 16, 6, 1.75F);
			this.body.setRotationPoint(0.0F, 5.0F, 2.0F);
			float f = 0.5F;
			this.leg1 = new ModelRenderer(this, 0, 16);
			this.leg1.addBox(-2.0F, 0.0F, -2.0F, 4, 6, 4, f);
			this.leg1.setRotationPoint(-3.0F, 12.0F, 7.0F);
			this.leg2 = new ModelRenderer(this, 0, 16);
			this.leg2.addBox(-2.0F, 0.0F, -2.0F, 4, 6, 4, f);
			this.leg2.setRotationPoint(3.0F, 12.0F, 7.0F);
			this.leg3 = new ModelRenderer(this, 0, 16);
			this.leg3.addBox(-2.0F, 0.0F, -2.0F, 4, 6, 4, f);
			this.leg3.setRotationPoint(-3.0F, 12.0F, -5.0F);
			this.leg4 = new ModelRenderer(this, 0, 16);
			this.leg4.addBox(-2.0F, 0.0F, -2.0F, 4, 6, 4, f);
			this.leg4.setRotationPoint(3.0F, 12.0F, -5.0F);
		}
		else if (isSheep == 2)
		{
			this.head = new ModelRenderer(this, 0, 0);
			this.head.addBox(-3.0F, -4.0F, -6.0F, 6, 6, 8, 0.0F);
			this.head.setRotationPoint(0.0F, 6.0F, -8.0F);
			this.body = new ModelRenderer(this, 28, 8);
			this.body.addBox(-4.0F, -10.0F, -7.0F, 8, 16, 6, 0.0F);
			this.body.setRotationPoint(0.0F, 5.0F, 2.0F);
		}
	}

	@Override
	public void setRotationAngles(float par1, float par2, float par3, float par4, float par5, float par6, Entity par7Entity)
	{
		this.body.rotateAngleX = (float) Math.PI / 2F;
	}
}
