package com.slatebit.stuffedanimals.common;

import io.netty.buffer.ByteBuf;
import java.util.Random;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.passive.EntityCow;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;
import cpw.mods.fml.common.registry.IEntityAdditionalSpawnData;

public class EntityStuffedAnimal extends EntityLiving implements IEntityAdditionalSpawnData
{
	public int type;
	public boolean shouldMirror = false;
	public int texture = -1;
	private boolean hasFaced = false;

	public EntityStuffedAnimal(World world)
	{
		super(world);
		this.setHealth(1);
		this.setSize();
	}

	public EntityStuffedAnimal(World world, int type, EntityPlayer player)
	{
		super(world);
		this.type = type;
		this.setHealth(1);
		this.setSize();
		if (type == 7 || type == 10)
		{
			Random random = new Random();
			int size = type == 7 ? 11 : 6;
			String index = type + "0" + (random.nextInt(size) + 1);
			this.texture = Integer.parseInt(index);
		}
	}

	@Override
	public void onUpdate()
	{
		super.onUpdate();
		this.setSize();
		this.addRandomArmor();
		EntityPlayer player = this.findPlayer();
		if (player != null && !this.hasFaced)
		{
			this.faceEntity(player, 360F, 360F);
			if (this.rotationYaw >= 0F && this.rotationYaw < 135F)
			{
				this.rotationYaw += 45F;
			}
			else if (this.rotationYaw < 0F && this.rotationYaw >= -135F)
			{
				this.rotationYaw -= 45F;
			}
			else if (this.rotationYaw > 135F)
			{
				this.rotationYaw += 135F;
				this.shouldMirror = true;
			}
			else if (this.rotationYaw < -135F)
			{
				this.rotationYaw -= 135F;
				this.shouldMirror = true;
			}
			this.hasFaced = true;
		}
		if (this.type == 2 && this.getCustomNameTag().equals("PokemonDan11"))
		{
			if (!this.worldObj.isRemote)
			{
				EntityCow entityCow = new EntityCow(this.worldObj);
				entityCow.setLocationAndAngles(this.posX, this.posY, this.posZ, this.rotationYaw, 0.0F);
				entityCow.setCustomNameTag("PokemonDan11");
				entityCow.addPotionEffect(new PotionEffect(Potion.resistance.id, 2147483647, 100, false));
				this.worldObj.spawnEntityInWorld(entityCow);
				entityCow.playLivingSound();
			}
			double var4 = this.rand.nextGaussian() * 0.02D;
			double var6 = this.rand.nextGaussian() * 0.02D;
			double var8 = this.rand.nextGaussian() * 0.02D;
			this.worldObj.spawnParticle("happyVillager", this.posX + this.rand.nextFloat() * this.width * 2.0F - this.width, this.posY + 0.5D + this.rand.nextFloat() * this.height, this.posZ + this.rand.nextFloat() * this.width * 2.0F - this.width, var4, var6, var8);
			this.worldObj.spawnParticle("happyVillager", this.posX + this.rand.nextFloat() * this.width * 2.0F - this.width, this.posY + 0.5D + this.rand.nextFloat() * this.height, this.posZ + this.rand.nextFloat() * this.width * 2.0F - this.width, var4, var6, var8);
			this.worldObj.spawnParticle("happyVillager", this.posX + this.rand.nextFloat() * this.width * 2.0F - this.width, this.posY + 0.5D + this.rand.nextFloat() * this.height, this.posZ + this.rand.nextFloat() * this.width * 2.0F - this.width, var4, var6, var8);
			this.worldObj.spawnParticle("happyVillager", this.posX + this.rand.nextFloat() * this.width * 2.0F - this.width, this.posY + 0.5D + this.rand.nextFloat() * this.height, this.posZ + this.rand.nextFloat() * this.width * 2.0F - this.width, var4, var6, var8);
			this.worldObj.spawnParticle("happyVillager", this.posX + this.rand.nextFloat() * this.width * 2.0F - this.width, this.posY + 0.5D + this.rand.nextFloat() * this.height, this.posZ + this.rand.nextFloat() * this.width * 2.0F - this.width, var4, var6, var8);
			this.setDead();
		}
	}

	@Override
	public boolean interact(EntityPlayer entityPlayer)
	{
		if (entityPlayer.getCurrentEquippedItem() == null || entityPlayer.getCurrentEquippedItem() != null && entityPlayer.getCurrentEquippedItem().getItem() != Items.name_tag)
		{
			this.playSound();
			double var4 = this.rand.nextGaussian() * 0.02D;
			double var6 = this.rand.nextGaussian() * 0.02D;
			double var8 = this.rand.nextGaussian() * 0.02D;
			this.worldObj.spawnParticle("heart", this.posX + this.rand.nextFloat() * this.width * 2.0F - this.width, this.posY + 0.5D + this.rand.nextFloat() * this.height, this.posZ + this.rand.nextFloat() * this.width * 2.0F - this.width, var4, var6, var8);
			this.worldObj.spawnParticle("heart", this.posX + this.rand.nextFloat() * this.width * 2.0F - this.width, this.posY + 0.5D + this.rand.nextFloat() * this.height, this.posZ + this.rand.nextFloat() * this.width * 2.0F - this.width, var4, var6, var8);
			this.worldObj.spawnParticle("heart", this.posX + this.rand.nextFloat() * this.width * 2.0F - this.width, this.posY + 0.5D + this.rand.nextFloat() * this.height, this.posZ + this.rand.nextFloat() * this.width * 2.0F - this.width, var4, var6, var8);
			this.worldObj.spawnParticle("heart", this.posX + this.rand.nextFloat() * this.width * 2.0F - this.width, this.posY + 0.5D + this.rand.nextFloat() * this.height, this.posZ + this.rand.nextFloat() * this.width * 2.0F - this.width, var4, var6, var8);
			this.worldObj.spawnParticle("heart", this.posX + this.rand.nextFloat() * this.width * 2.0F - this.width, this.posY + 0.5D + this.rand.nextFloat() * this.height, this.posZ + this.rand.nextFloat() * this.width * 2.0F - this.width, var4, var6, var8);
			return true;
		}
		return false;
	}

	@Override
	protected void updateEntityActionState()
	{
		++ this.entityAge;
		this.despawnEntity();
	}

	@Override
	public void knockBack(Entity p_70653_1_, float p_70653_2_, double p_70653_3_, double p_70653_5_)
	{
		if (this.rand.nextDouble() >= this.getEntityAttribute(SharedMonsterAttributes.knockbackResistance).getAttributeValue())
		{
			this.isAirBorne = true;
			this.motionY += 0.4;
		}
	}

	private void playSound()
	{
		int type = this.type == 7 && this.texture > -1 ? this.texture : this.type;
		this.worldObj.playSoundAtEntity(this, StuffedAnimals.SOUND_MAP.get(type), this.getSoundVolume(), this.getSoundPitch());
	}

	private EntityPlayer findPlayer()
	{
		EntityPlayer player = this.worldObj.getClosestPlayerToEntity(this, 16D);
		if (player != null && this.canEntityBeSeen(player))
		{
			return player;
		}
		else
		{
			return null;
		}
	}

	private void setSize()
	{
		if (this.type == 0 || this.type == 7 || this.type == 10 || this.type == 15 || this.type == 16 || this.type == 17 || this.type == 21 || this.type == 24 || this.type == 25)
		{
			this.setSize(0.3F, 0.9F);
		}
		else if (this.type == 1)
		{
			this.setSize(0.225F, 0.525F);
		}
		else if (this.type == 2 || this.type == 3 || this.type == 6 || this.type == 29)
		{
			this.setSize(0.45F, 0.65F);
		}
		else if (this.type == 4 || this.type == 14)
		{
			this.setSize(0.3F, 0.4F);
		}
		else if (this.type == 5 || this.type == 8 || this.type == 18 || this.type == 19 || this.type == 20 || this.type == 22 || this.type == 28)
		{
			this.setSize(0.45F, 0.45F);
		}
		else if (this.type == 9)
		{
			this.setSize(0.25F, 0.45F);
		}
		else if (this.type == 11)
		{
			this.setSize(0.35F, 0.25F);
		}
		else if (this.type == 12)
		{
			this.setSize(0.7F, 0.45F);
		}
		else if (this.type == 13)
		{
			this.setSize(0.3F, 1.45F);
		}
		else if (this.type == 26)
		{
			this.setSize(0.2F, 0.9F);
		}
		else if (this.type == 27)
		{
			this.setSize(0.7F, 1.45F);
		}
	}

	@Override
	protected void addRandomArmor()
	{
		if (this.type == 15)
		{
			this.setCurrentItemOrArmor(0, new ItemStack(Items.golden_sword));
		}
		else if (this.type == 21)
		{
			this.setCurrentItemOrArmor(0, new ItemStack(Items.bow));
		}
		else if (this.type == 23)
		{
			this.setCurrentItemOrArmor(0, new ItemStack(Items.stone_sword));
		}
	}

	@Override
	protected void dropEquipment(boolean p_82160_1_, int p_82160_2_)
	{
	}

	@Override
	protected void dropFewItems(boolean par1, int par2)
	{
		this.entityDropItem(new ItemStack(StuffedAnimals.stuffedAnimal, 1, this.type), 1);
	}

	@Override
	public void writeSpawnData(ByteBuf data)
	{
		data.writeInt(this.type);
		data.writeBoolean(this.shouldMirror);
		data.writeBoolean(this.hasFaced);
		data.writeFloat(this.rotationYaw);
		data.writeInt(this.texture);
	}

	@Override
	public void readSpawnData(ByteBuf data)
	{
		this.type = data.readInt();
		this.shouldMirror = data.readBoolean();
		this.hasFaced = data.readBoolean();
		this.rotationYaw = data.readFloat();
		this.texture = data.readInt();
	}

	@Override
	public void writeEntityToNBT(NBTTagCompound nbt)
	{
		super.writeEntityToNBT(nbt);
		nbt.setInteger("Type", this.type);
		nbt.setBoolean("Should Mirror", this.shouldMirror);
		nbt.setBoolean("Has Faced", this.hasFaced);
		nbt.setFloat("Yaw", this.rotationYaw);
		nbt.setInteger("Texture", this.texture);
	}

	@Override
	public void readEntityFromNBT(NBTTagCompound nbt)
	{
		super.readEntityFromNBT(nbt);
		this.type = nbt.getInteger("Type");
		this.shouldMirror = nbt.getBoolean("Should Mirror");
		this.hasFaced = nbt.getBoolean("Has Faced");
		this.rotationYaw = nbt.getFloat("Yaw");
		this.texture = nbt.getInteger("Texture");
	}
}
