package com.slatebit.stuffedanimals.common;

import java.util.List;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class CreativeTab extends CreativeTabs
{
	CreativeTab(int tabIndex, String tabLabel)
	{
		super(tabIndex, tabLabel);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void displayAllReleventItems(List list)
	{
		list.add(new ItemStack(StuffedAnimals.present, 1, 0));
		for (int i = 0; i < StuffedAnimals.NAME_MAP.size(); i ++)
		{
			list.add(new ItemStack(StuffedAnimals.stuffedAnimal, 1, i));
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public Item getTabIconItem()
	{
		return StuffedAnimals.present;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public String getTranslatedTabLabel()
	{
		return this.getTabLabel();
	}
}