package com.slatebit.stuffedanimals.common;

import java.util.Random;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemPresent extends Item
{
	private IIcon icon;

	public ItemPresent()
	{
		this.setCreativeTab(StuffedAnimals.TAB);
		GameRegistry.registerItem(this, "Present");
	}

	@Override
	public ItemStack onItemRightClick(ItemStack itemStack, World world, EntityPlayer entityPlayer)
	{
		Random random = new Random();
		int size = StuffedAnimals.NAME_MAP.size();
		if (!world.isRemote)
		{
			entityPlayer.entityDropItem(new ItemStack(StuffedAnimals.stuffedAnimal, 1, random.nextInt(size)), 1);
			entityPlayer.entityDropItem(new ItemStack(StuffedAnimals.stuffedAnimal, 1, random.nextInt(size)), 1);
			entityPlayer.entityDropItem(new ItemStack(StuffedAnimals.stuffedAnimal, 1, random.nextInt(size)), 1);
		}
		InventoryPlayer inventory = entityPlayer.inventory;
		if (inventory.getCurrentItem() != null && inventory.getCurrentItem().getItem() instanceof ItemPresent && !entityPlayer.capabilities.isCreativeMode)
		{
			inventory.consumeInventoryItem(StuffedAnimals.present);
		}
		return itemStack;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIconFromDamage(int metadata)
	{
		return this.icon;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public String getItemStackDisplayName(ItemStack itemStack)
	{
		return "Present";
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister)
	{
		this.icon = iconRegister.registerIcon("stuffedanimals:Present");
	}
}
