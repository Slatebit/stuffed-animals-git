package com.slatebit.stuffedanimals.common;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Facing;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemStuffedAnimal extends Item
{
	private IIcon[] icons;

	public ItemStuffedAnimal()
	{
		this.setHasSubtypes(true);
		this.setCreativeTab(StuffedAnimals.TAB);
		GameRegistry.registerItem(this, "Stuffed <Type>");
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIconFromDamage(int metadata)
	{
		return this.icons[metadata];
	}

	@Override
	public boolean onItemUse(ItemStack itemStack, EntityPlayer entityPlayer, World world, int x, int y, int z, int a, float b, float c, float d)
	{
		if (!world.isRemote)
		{
			Block block = world.getBlock(x, y, z);
			x += Facing.offsetsXForSide[a];
			y += Facing.offsetsYForSide[a];
			z += Facing.offsetsZForSide[a];
			double offset = 0D;
			if (a == 1 && block.getRenderType() == 11)
			{
				offset = 0.5D;
			}
			EntityStuffedAnimal stuffedAnimal = this.spawnEntity(world, itemStack.getItemDamage(), x + 0.5D, y + offset, z + 0.5D, entityPlayer);
			if (stuffedAnimal != null && !entityPlayer.capabilities.isCreativeMode)
			{
				-- itemStack.stackSize;
			}
		}
		return true;
	}

	private EntityStuffedAnimal spawnEntity(World world, int metadata, double x, double y, double z, EntityPlayer player)
	{
		EntityStuffedAnimal stuffedAnimal = new EntityStuffedAnimal(world, metadata, player);
		if (stuffedAnimal != null)
		{
			stuffedAnimal.setLocationAndAngles(x, y, z, stuffedAnimal.rotationYaw, 0.0F);
			world.spawnEntityInWorld(stuffedAnimal);
			stuffedAnimal.playLivingSound();
		}
		return stuffedAnimal;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public String getItemStackDisplayName(ItemStack itemStack)
	{
		return "Stuffed " + StuffedAnimals.NAME_MAP.get(itemStack.getItemDamage());
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister)
	{
		int size = StuffedAnimals.NAME_MAP.size();
		this.icons = new IIcon[size];
		for (int i = 0; i < size; i ++)
		{
			this.icons[i] = iconRegister.registerIcon("stuffedanimals:" + StuffedAnimals.NAME_MAP.get(i));
		}
	}
}
